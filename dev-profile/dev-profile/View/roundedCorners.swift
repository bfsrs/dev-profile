//
//  roundedCorners.swift
//  dev-profile
//
//  Created by benny franke on 22.01.18.
//  Copyright © 2018 showRescueService. All rights reserved.
//

import UIKit

class roundedCorners: UIImageView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 7
        layer.masksToBounds = true
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
